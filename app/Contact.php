<?php

namespace App;


class Contact
{

	private string $name;
	private string $number;
	
	function __construct($name, $number)
	{
		$this->$name = $name;
		$this->$number = $number;
	}

	public function getName(){
		return $name;
	}

	public function getNumber(){
		return $number;
	}

	public function setName($name){
		$this->$name = $name;
	}

	public function setNumber($number){
		$this->$number = $number;
	}
}