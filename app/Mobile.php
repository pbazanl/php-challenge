<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		/*
		$contactService = Mockery::mock('contactService');
		$contactService->shouldReceive('findByName')
            ->times(1)
            ->andReturn(new Contact("Jose Perez","999888777"));

		$contact = new ContactService::findByName($contactService);
		*/

		$contact = ContactService::findByName($name);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}


	public function validateExistByName($name = '')
	{
		if( empty($name) ) return;

		$exists = ContactService::existByName($name);

		return $exists;
	}


	public function sendSMS($number = '', $body = '')
	{
		if( empty($number) ) return;
		if( empty($body) ) return;

		if(!ContactService::validateNumber($number)){
			return false;
		}

		$contact = ContactService::findByNumber($number);

		return $this->provider->sendSMS($contact, $body);
	}


}
