<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\Interfaces\CarrierInterface;
use App\Interfaces\Implementation\SamsungGalaxyS20Implementation;
use App\Interfaces\Implementation\SamsungGalaxyA50Implementation;
use App\Interfaces\Implementation\iphone13miniImplementation;
use App\Services\ContactService;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{

		$provider = new SamsungGalaxyS20Implementation();

		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}
	
	/** @test */
	public function make_call_by_name_with_valid_contact($name = "Jose Perez")
	{

		$provider = new SamsungGalaxyA50Implementation();

		$mobile = new Mobile($provider);

		$this->assertGreaterThan(1,strlen($name));

		$this->assertNotNull($mobile->makeCallByName($name));
	}
	
	/** @test */
	public function make_call_by_name_with_not_found_contact($name = "Place Holder")
	{

		$provider = new iphone13miniImplementation();

		$mobile = new Mobile($provider);

		$exists = $mobile->validateExistByName($name);

		$this->assertFalse($exists);
	}
	
	/** @test */
	public function send_sms_by_number($number = "666555444", $body = "sms body")
	{

		$provider = new SamsungGalaxyS20Implementation();

		$mobile = new Mobile($provider);

		$this->assertTrue($mobile->sendSMS($number, $body));
	}

}
