<?php

namespace App\Interfaces\Implementation;

use App\Interfaces\CarrierInterface;
use App\Contact;
use App\Call;


class SamsungGalaxyA50Implementation implements CarrierInterface
{
	
	public function dialContact(Contact $contact){
		// do something
		//$number = $contact->getNumber();
		//print_r("Dialing: " + $number);
	}

	public function makeCall(): Call{
		// do something
		return new Call();
	}

	public function sendSMS(Contact $contact, string $body): bool{
		return true;
	}
}