<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName(string $name): Contact
	{
		// queries to the db
		if($name === "Jose Perez"){
			return new Contact("Jose Perez","999888777");
		}
	}
	
	public static function findByNumber(string $number): Contact
	{
		// queries to the db
		if($number === "666555444"){
			return new Contact("Jhon Smith","666555444");
		}
	}
	
	public static function existByName(string $name): bool
	{
		// queries to the db
		if($name == "Jose Perez"){
			return true;
		}else{
			return false;
		}
	}

	//solo numeros de celulares peruanos
	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
		return preg_match('/^[0-9]{9}+$/', $number);
	}
}